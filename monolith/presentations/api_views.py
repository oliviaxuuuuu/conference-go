from django.http import JsonResponse
from .models import Presentation,Status
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
from events.models import Conference

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["presenter_name","title"]

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]
    def get_extra_data(self, o):
        return { "status": o.status.name }

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method== "GET":
        presentation = Presentation.objects.all()
        return JsonResponse(
            {"presentation":presentation},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            content["conference"]=Conference.objects.get(id=conference_id)
            content["status"]= Status.objects.get(name="SUBMITTED")
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        presentation = Presentation.objects.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method== "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )